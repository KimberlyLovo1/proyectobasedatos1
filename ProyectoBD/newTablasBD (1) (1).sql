create database BD_NGOCIO_TAMA
USE BD_NGOCIO_TAMA





CREATE TABLE PROVEEDOR(
ID_PROV INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
NOMBRE_PROV NVARCHAR(30) NOT NULL,
DIR_PROV NVARCHAR(60) NOT NULL,
TELP CHAR(8),
CORREO NVARCHAR(20) NOT NULL
)
ALTER TABLE [dbo].[PROVEEDOR]  WITH CHECK ADD CHECK  (([TELP] like '[2|5|7|8][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'))
GO


CREATE TABLE INVENTARIO(
DESCRIP NVARCHAR(70) NOT NULL,
NOMBRE_PROD NVARCHAR(30) NOT NULL,
PRECIO FLOAT NOT NULL,
EXISTP INT NOT NULL,
ID_PROV INT FOREIGN KEY REFERENCES PROVEEDOR(ID_PROV) NOT NULL,
COD_PROD CHAR(5) PRIMARY KEY
)
go

Create table COMPRA(
ID_COMPRA int identity(1,1) primary key not null,
TOTALC FLOAT not null,
FECHA_COMPRA DATE NOT NULL
)
go
Create Table DETALLE_COMPRA(
ID_COMPRA INT FOREIGN KEY REFERENCES COMPRA(ID_COMPRA) NOT NULL,
COD_PROD CHAR(5) FOREIGN KEY REFERENCES INVENTARIO(COD_PROD) NOT NULL,
CANTV FLOAT NOT NULL,
COSTO float not null,
SUBTV FLOAT NOT NULL,
PRIMARY KEY(ID_COMPRA,COD_PROD)
)
go
Create table VENTA(
Nombre_Cliente Nvarchar(20),
ID_VENTA int identity(1,1) primary key  not null,
FECHA_COMPRA DAte not null,
TOTALV float not null,
TIPO_PAGO NVARCHAR(30) NOT NULL
)
go

Create Table DETALLE_VENTAS(
ID_VENTA INT FOREIGN KEY REFERENCES VENTA(ID_VENTA) NOT NULL,
COD_PROD CHAR(5) FOREIGN KEY REFERENCES INVENTARIO(COD_PROD) NOT NULL,
CANTV FLOAT NOT NULL,
SUBTV FLOAT NOT NULL,
PRIMARY KEY(ID_VENTA,COD_PROD)
)
go


CREATE TABLE ESTADO_RESULT(
ID_ESTADO_RESULT int identity(1,1) primary key not null,
VENTAS FLOAT NOT NULL ,
COSTO_VENTAS FLOAT NOT NULL,
UTILIDAD_BRUTA FLOAT NOT NULL,
GASTOS_ADM FLOAT NOT NULL,
GASTOS_VENTA FLOAT NOT NULL,
GASTOS_FINANCIEROS FLOAT NOT NULL,
UTILIDAD_OPERACION FLOAT NOT NULL,
FECHA_INICIO DATE NOT NULL,
FECHA_FINAL DATE NOT NULL
);
go
CREATE TABLE GASTOS(
ID_GASTOS int identity(1,1) primary key NOT NULL,
NOMBRE nvarchar(20) not null,
DESCRIPCION nvarchar(60) not null,
TIPO_GASTO NVARCHAR(20) NOT NULL,
COSTO FLOAT NOT NULL,
FECHA_INICIO DATE NOT NULL,
FECHA_FINAL DATE NOT NULL
)
go
CREATE TABLE DEUDORES(
ID_DEUD INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
NOMBRE NVARCHAR(20) NOT NULL,
DIRECCION NVARCHAR(60) NOT NULL,
ID_VENTA INT FOREIGN KEY REFERENCES VENTA(ID_VENTA) NOT NULL,
FECHA_CANCELA DATE NOT NULL
)
go


--metodos de insercion de compra venta, compra, inventarios y proveeedores

create procedure ADD_VENTAS
@nomb nvarchar(20),
@fech date,
@TIPO_PAGO NVARCHAR(30)
as
begin
declare @id int
insert into venta values(@nomb,@fech,0, @TIPO_PAGO)
set @id= SCOPE_IDENTITY()
select * from venta where ID_VENTA=@id
end
GO
--se seteo el dato para obtener la lista de todos las ventas

create procedure ADD_COMPRA
@fech date
as
begin
declare @id int
insert into COMPRA VALUES(0,@fech)
set @id= SCOPE_IDENTITY()
select * from COMPRA where ID_COMPRA=@id
end
GO
--se seteo el dato para obtener la lista de todos las compras

--procedimiento para gregar producto
CREATE procedure ADD_PRODUCTO
@Desc nvarchar(70),
@nomb nvarchar(30),
@precio float,
@idprov int,
@codprod char(5),
@Exist int
as
declare @idp as int
set @idp=(select ID_PROV from PROVEEDOR where ID_PROV=@idprov)
if(@idprov=@idp)
begin 
insert into INVENTARIO values (@Desc,@nomb,@precio,@Exist,@idprov,@codprod)
end
else
begin
print'proveedor no existente'
end
go


--metodo de agregar un nuevo PROVEEDOR
create procedure ADD_PROVEEDOR
@nomb nvarchar(30),
@dir nvarchar(60),
@Telp char(8),
@correo nvarchar(20)
as
declare @te as char(8)
set @te=''
if(@Telp=@te)
begin
insert into PROVEEDOR values(@nomb,@dir,'',@correo)
end
else
begin
insert into PROVEEDOR values(@nomb,@dir,@Telp,@correo)
end
go
--se agrego un nuevo proveedor

--crear metodos de insercion de detalle de ventas y detalle compras
--triger para actualizar existencia en inventario de detalle de venta y detalle de compra, ttambien triger para actualizar
--total en ventas y compra cuando introduzcan dettalle venta y detalle compra y tambien actualizar precio de venta
create procedure MostrarGastos
as
select *from GASTOS
go
create procedure MostrarProductos
as
Select * from INVENTARIO
GO
create procedure MostrarProveedores
as
Select * from PROVEEDOR
GO
create procedure MostrarDeudores
as
Select * from DEUDORES
go
create procedure MostrarCompra
as
Select * from COMPRA
go
create procedure MostrarVentas
as
Select * from venta
go
CREATE proc MOSTRAR_DETALLE_compra
@idCompra int
AS
SELECT *FROM  Detalle_compra where ID_COMPRA = @idCompra
GO
create proc MOSTRAR_DETALLE_VENTA
@id int
AS
SELECT *FROM  Detalle_ventas where ID_VENTA=@id
GO
create proc MostrarER
AS
SELECT *FROM ESTADO_RESULT
GO

--TRIGGER PARA ACTUALIZAR TOTAL COMPRAS

CREATE trigger Actualizar_total_compras
on Detalle_compra
AFTER insert 
as
DECLARE @idusuario as int
DECLARE @cod_prod as char(5)
set @idusuario=(select ID_COMPRA FROM INSERTED)
set @cod_prod=(select COD_PROD FROM INSERTED )
UPDATE COMPRA SET TOTALC =TOTALC +(SELECT SUBTV FROM INSERTED) WHERE ID_COMPRA=@IdUsuario
UPDATE INVENTARIO SET  EXISTP=EXISTP +(SELECT CANTV FROM INSERTED) WHERE COD_PROD=@cod_prod
GO
--TRIGGER PARA ACTUALIZAR TOTAL VENTAS Y EXISTENCIAS EN INVENTARIO

CREATE trigger Actualizar_total_ventas
on Detalle_ventas
AFTER insert 
as
DECLARE @idusuario as int
DECLARE @cod_prod as char(5)
set @idusuario=(select Id_Venta FROM INSERTED)
set @cod_prod=(select COD_PROD FROM INSERTED )
UPDATE VENTA SET TOTALv =TOTALV+(SELECT SUBTV FROM inserted) WHERE ID_VENTA=@idusuario
UPDATE INVENTARIO SET EXISTP= EXISTP-(SELECT CANTV FROM INSERTED) WHERE COD_PROD=@cod_prod
 GO
 --INGRESAR  DETALLE DE COMPRA
CREATE proc ADD_DETALLE_COMPRA
@idCom int,
@codprod char(5),
@cant int,
@cost float,
@subt float
as
declare @id as int
set @id=(select ID_COMPRA FROM COMPRA where ID_COMPRA=@idcom)
declare @codpr as char(5)
set @codpr=(select COD_PROD from INVENTARIO where COD_PROD=@codprod)

if(@cant>0)
begin
if(@idcom=@id and @codprod=@codpr)
begin

insert into Detalle_compra(ID_COMPRA,COD_PROD,CANTV,COSTO,SUBTV) values(@idcom,@codprod,@cant,@cost,@subt)
end
else
begin
print'codigo errado'
end
end
else
begin
print'no puede ser cantidad 0'
end
GO
--INGRESAR DETALLE DE VENTA
CREATE proc ADD_DETALLE_VENTA
@idCom int,
@codprod char(5),
@cant float
as
declare @id as int
set @id=(select ID_VENTA FROM VENTA where ID_VENTA=@idcom)
declare @codpr as char(5)
set @codpr=(select COD_PROD from INVENTARIO where COD_PROD=@codprod)

if(@cant>0)
begin
if(@idcom=@id and @codprod=@codpr)
begin
DECLARE @PRECIO AS FLOAT
SET @PRECIO=(SELECT PRECIO FROM INVENTARIO WHERE COD_PROD=@codprod )
insert into Detalle_ventas(ID_VENTA,COD_PROD,CANTV,SUBTV) values(@idcom,@codprod,@cant,@cant*@PRECIO)
end
else
begin
print'codigo errado'
end
end
else
begin
print'no puede ser cantidad 0'
end

GO

--METODOS UPDATE PARA LAS TBALAS INVENTARIO, PROVEEDORE, DEUDORES
--**UPDATE INVENTARIO
create procedure UPDATE_INV
@DESCP NVARCHAR(70),
@NOMBRE_PRO NVARCHAR(30),
@PRECIO FLOAT,
@EXISTP INT,
@IDPROV INT,
@COD CHAR(5)
AS
UPDATE INVENTARIO SET DESCRIP=@DESCP ,NOMBRE_PROD=@NOMBRE_PRO, PRECIO=@PRECIO, EXISTP=@EXISTP, ID_PROV=@IDPROV WHERE COD_PROD=@COD 
GO
--**UPDATE GASTOS
CREATE PROCEDURE UPDATE_GASTO
@IDGAST INT,
@NOMMB NVARCHAR(20),
@DESCP NVARCHAR(60),
@TIPO NVARCHAR(20),
@COST FLOAT
AS
UPDATE GASTOS SET NOMBRE=@NOMMB, DESCRIPCION=@DESCP , TIPO_GASTO=@TIPO,COSTO=@COST WHERE ID_GASTOS=@IDGAST
GO

--**UPDATE DEUDORES
CREATE PROCEDURE UPDATE_DEUDORES
@IDDEUD INT,
@NOMB NVARCHAR(20),
@DIRECCION NVARCHAR(60),
@IDVENT INT,
@FECH DATE
AS
UPDATE DEUDORES SET NOMBRE=@NOMB, DIRECCION=@DIRECCION, ID_VENTA=@IDVENT, FECHA_CANCELA=@FECH WHERE ID_DEUD=@IDDEUD
GO
--**update para proveedores
CREATE PROCEDURE UPDATE_Proveedores
@IDDEUD INT,
@NOMB NVARCHAR(20),
@DIRECCION NVARCHAR(60),
@tel char(4),
@correo nvarchar(20)
AS
UPDATE PROVEEDOR SET NOMBRE_PROV=@NOMB, DIR_PROV=@DIRECCION, TELP=@tel, CORREO=@correo WHERE ID_PROV=@IDDEUD
GO

--**UPDATE PARA ALIMETAR EL ER ACTUAL
CREATE PROCEDURE UPDATE_ER
@IDEST INT,
@VENTAS FLOAT,
@COSTO_VENTAS FLOAT ,
@GASTOS_ADM FLOAT,
@GASTOS_VENTA FLOAT,
@GASTOS_FINANCIEROS FLOAT 
AS
UPDATE ESTADO_RESULT SET VENTAS=VENTAS+@VENTAS, COSTO_VENTAS=COSTO_VENTAS+ @COSTO_VENTAS,UTILIDAD_BRUTA=UTILIDAD_BRUTA+ @VENTAS-@COSTO_VENTAS,GASTOS_ADM=GASTOS_ADM+ @GASTOS_ADM,
GASTOS_FINANCIEROS=GASTOS_FINANCIEROS+ @GASTOS_FINANCIEROS,GASTOS_VENTA=GASTOS_VENTA+ @GASTOS_VENTA,UTILIDAD_OPERACION=UTILIDAD_OPERACION+ @VENTAS-@COSTO_VENTAS-@GASTOS_ADM-@GASTOS_FINANCIEROS-@GASTOS_VENTA
WHERE ID_ESTADO_RESULT=@IDEST
GO
--creandi un estado de resultador defaul aun por alimentarse
CREATE PROC DEFAULT_ER
@FECHIN DATE,
@FECHFIN DATE
AS
INSERT INTO ESTADO_RESULT VALUES(0,0,0,0,0,0,0,@FECHIN,@FECHFIN)
GO

--PROCEDIMIENTO PARA LISTAR TODOS LOS ESTADOS DE RESULTADO
create proc MostrarEstR
as
select *from ESTADO_RESULT

--CREANDO METODOS ELIMINAR DE VENTAS Y COSTO DE VENTAS
--**METODOS DE ELIMINAR VENTA
CREATE PROCEDURE DELETE_VENT
@IDVENT INT
AS
DELETE FROM VENTA WHERE ID_VENTA=@IDVENT
DELETE FROM Detalle_ventas WHERE Id_Venta=@IDVENT
GO
--*METODO ELIMINAR COMPRA
CREATE PROCEDURE DELETE_COMPRA
@IDVENT INT
AS
DELETE FROM Compra WHERE ID_Compra=@IDVENT
DELETE FROM DETALLE_COMPRA WHERE ID_COMPRA=@IDVENT
GO
--TRGGER PARA ELIMINAR EL VALOR COSTO VENTAS DE LA ELIMINACION DE COMPRA
CREATE TRIGGER ACT_ER_COMPRA
ON COMPRA
AFTER DELETE
AS
UPDATE ESTADO_RESULT SET COSTO_VENTAS=COSTO_VENTAS-(SELECT TOTALC FROM DELETED)
GO
--TRGGER PARA ELIMINAR EL VALOR VENTAS DE LA ELIMINACION DE VENTAS
CREATE TRIGGER ACT_ER_VENTA
ON VENTA
AFTER DELETE
AS
UPDATE ESTADO_RESULT SET VENTAS=VENTAS-(SELECT  TOTALV FROM DELETED)
GO

--agregar deudores
create procedure ADD_DEUDORES
@nomb nvarchar(30),
@dir nvarchar(60),
@fetch date,
@idVenta int
as
declare @idD as int
set @idD=(select ID_VENTA from DETALLE_VENTAS where ID_VENTA=@idVenta)
if(@idVenta=@idD)
begin 
insert into DEUDORES values (@nomb, @dir, @idVenta, @fetch)
end
else
begin
print'Venta no existente'
end
go
--agreagar gastos
create procedure AddGasto
@NOMBRE nvarchar(20),
@DESCRIPCION nvarchar(60),
@TIPO_GASTO NVARCHAR(20),
@COSTO FLOAT,
@FECHA_INICIO DATE,
@FECHA_FINAL DATE 
as
insert into GASTOS values (@NOMBRE,@DESCRIPCION,@TIPO_GASTO,@COSTO,@FECHA_INICIO,@FECHA_FINAL)
go
--VISTA DE PRODUCTO MAS VENDIDO
create view PRODUCTO_MAS_VENDIDO
as
SELECT TOP 1 Cantidad, Nombre FROM (
	SELECT Sum(cantv) AS cantidad, prt.NOMBRE_PROD as Nombre FROM Detalle_Ventas dv INNER JOIN INVENTARIO prt ON prt.Cod_Prod = dv.Cod_Prod GROUP BY prt.NOMBRE_PROD 
) A WHERE A.cantidad = 
(
	SELECT max(cantidad) FROM 
	(
		SELECT sum(cantv) AS cantidad, Cod_Prod FROM Detalle_Ventas GROUP BY Cod_Prod
	) D
)
GO

--ER del ultimo mes
Create view ER_ULTIMO
as
Select top 1 * from ESTADO_RESULT ORDER BY ID_ESTADO_RESULT Desc
go

create procedure ER_ULTIMOP
as
Select top 1 * from ESTADO_RESULT ORDER BY ID_ESTADO_RESULT Desc
go
