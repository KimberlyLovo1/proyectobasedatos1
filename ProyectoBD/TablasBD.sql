create database BD_NGOCIO_TAMA
USE BD_NGOCIO_TAMA

Create table ventas(
Nombre_Cliente Nvarchar(20),
ID_VENTA int identity(1,1) primary key  not null,
Fecha DAte not null,
TotalV float not null
)
drop table ventas

Create Table Detalle_ventas(
ID_VENTA INT FOREIGN KEY REFERENCES ventas(ID_VENTA) NOT NULL,
COD_PROD CHAR(5) FOREIGN KEY REFERENCES INVENTARIO(COD_PROD) NOT NULL,
CANTV FLOAT NOT NULL,
SUBTV FLOAT NOT NULL,
PRIMARY KEY(ID_VENTA,COD_PROD)

)
drop table Detalle_ventas

CREATE TABLE DEUDORES(
ID_DEUD INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
NOMBRE NVARCHAR(20) NOT NULL,
DIRECCION NVARCHAR(60) NOT NULL,
ID_VENTA INT FOREIGN KEY REFERENCES ventas(ID_VENTA) NOT NULL,
FECHA_CANCELA DATE NOT NULL
)
drop table DEUDORES

CREATE TABLE PROVEEDOR(
ID_PROV INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
NOMBRE_PROV NVARCHAR(30) NOT NULL,
DIR_PROV NVARCHAR(60) NOT NULL,
TELP CHAR(8),
CORREO NVARCHAR(20) NOT NULL
)
ALTER TABLE [dbo].[PROVEEDOR]  WITH CHECK ADD CHECK  (([TELP] like '[2|5|7|8][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'))
GO
drop table PROVEEDOR

CREATE TABLE INVENTARIO(
DESCRIP NVARCHAR(70) NOT NULL,
NOMBRE_PROD NVARCHAR(30) NOT NULL,
PRECIO FLOAT NOT NULL,
EXISTP INT NOT NULL,
ID_PROV INT FOREIGN KEY REFERENCES PROVEEDOR(ID_PROV) NOT NULL,
COD_PROD CHAR(5) PRIMARY KEY
)
drop table INVENTARIO

Create table COMPRA(
ID_COMPRA int identity(1,1) primary key not null,
TOTALC FLOAT not null,
FECHA_COMPRA DATE NOT NULL
)

Create Table Detalle_compra(
ID_COMPRA INT FOREIGN KEY REFERENCES COMPRA(ID_COMPRA) NOT NULL,
COD_PROD CHAR(5) FOREIGN KEY REFERENCES INVENTARIO(COD_PROD) NOT NULL,
CANTV FLOAT NOT NULL,
SUBTV FLOAT NOT NULL,
PRIMARY KEY(ID_COMPRA,COD_PROD)
)
---yap
drop table Detalle_compra
drop table COMPRA

CREATE TABLE ESTADO_RESULT(
VENTAS FLOAT NOT NULL,
COSTO_VENTAS FLOAT NOT NULL,
GASTOS_ADM FLOAT NOT NULL,
GASTOS_VENTA FLOAT NOT NULL,
OTROS_GASTOS FLOAT NOT NULL
)
CREATE TABLE GASTOS_FIJOS(
DINERO FLOAT NOT NULL,
NOMBRE nvarchar(20) not null,
DESCRIPCION nvarchar(60) not null,
ESTADO BIT NOT NULL,
TIPO_GASTO INT NOT NULL
)
drop table GASTOS_FIJOS

--metodos de insercion de compra venta, compra, inventarios y proveeedores

create procedure ADD_VENTAS
@nomb nvarchar(20),
@fech date
as

insert into ventas values(@nomb,@fech,0)


create procedure ADD_COMPRA
@fech date
as
insert into COMPRA VALUES(0,@fech)




create procedure ADD_PRODUCTO
@Desc nvarchar(70),
@nomb nvarchar(30),
@precio float,
@idprov int,
@codprod char(5)
as
declare @idp as int
set @idp=(select ID_PROV from PROVEEDOR where ID_PROV=@idprov)
if(@idprov=@idp)
begin 
insert into INVENTARIO values (@Desc,@nomb,@precio,0,@idprov,@codprod)
end
else
begin
print'proveedor no existente'
end
delete from PROVEEDOR
Select * from PROVEEDOR

create procedure ADD_PROVEEDOR
@nomb nvarchar(30),
@dir nvarchar(60),
@Telp char(8),
@correo nvarchar(20)
as
declare @te as char(8)
set @te=''
if(@Telp=@te)
begin
insert into PROVEEDOR values(@nomb,@dir,'',@correo)
end
else
begin
insert into PROVEEDOR values(@nomb,@dir,@Telp,@correo)
end


--crear metodos de insercion de detalle de ventas y detalle compras
--triger para actualizar existencia en inventario de detalle de venta y detalle de compra, ttambien triger para actualizar
--total en ventas y compra cuando introduzcan dettalle venta y detalle compra y tambien actualizar precio de venta

create procedure MostrarProductos
as
Select * from INVENTARIO

create procedure MostrarProveedores
as
Select * from PROVEEDOR

create procedure MostrarDeudores
as
Select * from DEUDORES

create procedure MostrarCompra
as
Select * from COMPRA

create procedure MostrarVentas
as
Select * from ventas