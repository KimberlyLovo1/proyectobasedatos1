﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoBD.BaseDatos;

namespace ProyectoBD.Add
{
    public partial class AddGastos : Form
    {
        
        public AddGastos()
        {
            InitializeComponent();
            cmbTipo.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ( cmbTipo.SelectedIndex == -1 || txtNombre.Text.Equals("") || txtDescrip.Text.Equals("") || txtCosto.Text.Equals("")){
                MessageBox.Show("Por favor, rellenar los campos");
                return;
            }
            GASTOS_DALC.InsertGastos(txtNombre.Text, txtDescrip.Text, cmbTipo.SelectedItem.ToString(), float.Parse(txtCosto.Text));
            if(cmbTipo.SelectedItem.Equals("gastos admin"))
            {
                GASTOS_DALC.Update_er(laster(),0,0,0,0,double.Parse(txtCosto.Text));
            }
            if (cmbTipo.SelectedItem.Equals("gastos venta"))
            {
                GASTOS_DALC.Update_er(laster(), 0, 0, 0, double.Parse(txtCosto.Text), 0);
            }
            if (cmbTipo.SelectedItem.Equals("gastos Financieras"))
            {
                GASTOS_DALC.Update_er(laster(), 0, 0, double.Parse(txtCosto.Text), 0, 0);
            }
            MessageBox.Show("Agregado correctamente");
            this.Dispose();
            

        }
        public int laster()
        {
            int id = 0;
            List<MostrarER_Result> ep = GASTOS_DALC.listarEr();
            foreach (MostrarER_Result eop in ep)
            {
                id = eop.ID_ESTADO_RESULT;
            }
            return id;
        }

        private void cmbTipo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
