﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoBD.BaseDatos;

namespace ProyectoBD.Add
{
    public partial class AddVenta : Form
    {
        Validacion v = new Validacion();
        DateTime fech = DateTime.Now;

        public AddVenta()
        {
            InitializeComponent();
            txtFecha.Text = fech.ToString();
            loadcmbCod();
        }

        public void loadcmbCod()
        {
            List<MostrarProductos_Result> CargarProducto = PRODUCTO_DALC.CargarProductos();
            foreach (MostrarProductos_Result mp in CargarProducto)
            {
                cmbCodProd.Items.Add(mp.COD_PROD);
            }
        }

       

        private void btnFacturar_Click(object sender, EventArgs e)
        {
            if(dgvVentas.Rows.Count > 0)
            {
                int opl = 0;
                int ID_vent = VENTA_DALC.Insertar_venta(txtNombreCliente.Text, cmbTipoPago.SelectedItem.ToString());
                

                for (int fila = 0; fila < dgvVentas.Rows.Count - 1; fila++)
                {

                    opl = VENTA_DALC.Insertar_Detalleventa(ID_vent, dgvVentas.Rows[fila].Cells[0].Value.ToString(), Convert.ToInt32(dgvVentas.Rows[fila].Cells[2].Value.ToString()));

                }
                GASTOS_DALC.Update_er(laster(), double.Parse(txtTotal.Text), 0, 0, 0, 0);
                reset();
                MessageBox.Show("registrado correctamente");
                dgvVentas.Rows.Clear();
                txtExist.Text = actExist(cmbCodProd.SelectedItem.ToString());
                if (cmbTipoPago.SelectedIndex == 0)
                {
                    AddDeudores ad = new AddDeudores();
                    ad.Visible = true;
                }
                

            }
            
        }
        public int laster()
        {
            int id = 0;
            List<MostrarER_Result> ep = GASTOS_DALC.listarEr();
            foreach(MostrarER_Result eop in ep)
            {
                id = eop.ID_ESTADO_RESULT;
            }
            return id;
        }

        public string actExist(String codpros)
        {
            List<MostrarProductos_Result> list = PRODUCTO_DALC.CargarProductos();
            int exist = 0;
            foreach (MostrarProductos_Result ok in list)
            {
                if (codpros.Equals(ok.COD_PROD))
                {
                    exist = ok.EXISTP;
                }
            }
            return exist.ToString();

        }
        public void reset()
        {
            txtCantidad.Text = "";
            txtNombreCliente.Text = "";
            txtTotal.Text = "";
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (float.Parse(txtExist.Text) < float.Parse(txtCantidad.Text))
            {
                MessageBox.Show("cantidad requerida menor a la disponible");
                return;
            }
            if (txtCantidad.Text.Equals(""))
            {
                MessageBox.Show("ingrese todos los datos correctamente");
                return ;
            }
            double preci = consultPrec(cmbCodProd.SelectedItem.ToString());
            double ex = preci * float.Parse(txtCantidad.Text);
            for (int fila = 0; fila < dgvVentas.Rows.Count - 1; fila++)
            {
                if (dgvVentas.Rows[fila].Cells[0].Value.ToString() == cmbCodProd.SelectedItem.ToString())
                {
                    MessageBox.Show("Actualmente ya hay un producto con ese codigo, elimine y agregue otro");
                    return;
                }
            }
            dgvVentas.Rows.Add(cmbCodProd.SelectedItem.ToString(), preci, txtCantidad.Text, ex.ToString());
            reset();
            if (txtTotal.Text.Equals(""))
            {
                txtTotal.Text = ex.ToString();
            }
            else
            {
                ex = double.Parse(txtTotal.Text) + ex;
                txtTotal.Text = ex.ToString();
            }

        }
        public double consultPrec(string codpro)
        {
            double precio = 0.00;
            List<MostrarProductos_Result> list = PRODUCTO_DALC.CargarProductos();

            foreach (MostrarProductos_Result ok in list)
            {
                if (codpro.Equals(ok.COD_PROD))
                {
                    precio = ok.PRECIO;
                }
            }
            return precio;
        }

        private void txtCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirEnteros(e);
        }

        private void cmbTipoPago_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbTipoPago.SelectedIndex == 0)
            {
                AddDeudores ad = new AddDeudores();
                ad.Visible = true;
            }
        }

        private void cmbCodProd_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtExist.Text = actExist(cmbCodProd.SelectedItem.ToString());
        }

        private void txtNombreCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirLetras(e);
        }
    }
}
