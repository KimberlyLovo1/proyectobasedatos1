﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoBD.BaseDatos;

namespace ProyectoBD.Add
{
    public partial class AddProv : Form
    {
        public AddProv()
        {
            InitializeComponent();
            btnActualiza.Visible = false;
            txtID.Visible = false;
        } //VALIDACIONES APLICAR
        public void Actualiza(String id,String nomb,String dirprov,String telp,String correo)
        {
            btnAceptar.Visible = false;
            txtID.Text = id;
            txtName.Text = nomb;
            txtDirec.Text = dirprov;
            mtbTelef.Text = telp;
            txtCorreo.Text = correo;
            txtID.Visible = true;
            txtID.Enabled = false;
            btnActualiza.Visible = true;

        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
           if(txtName.Text == ""||
            txtDirec.Text == ""||
            mtbTelef.Text ==""||
            txtCorreo.Text == "")
            {
                MessageBox.Show("ingrese los datos completos porfavor", "mensaje");
                return;
            }
            PROVEEDOR_DALC.InsertarProveedores(txtName.Text, txtDirec.Text, mtbTelef.Text, txtCorreo.Text);
            reset();
        }

        public void reset()
        {
            txtName.Text = "";
            txtDirec.Text = "";
            mtbTelef.Text = "";
            txtCorreo.Text = "";

        }

        private void btnActualiza_Click(object sender, EventArgs e)
        {
            if (txtName.Text == "" ||
            txtDirec.Text == "" ||
            mtbTelef.Text == "" ||
            txtCorreo.Text == "")
            {
                MessageBox.Show("ingrese los datos completos porfavor", "mensaje");
                return;
            }
            PROVEEDOR_DALC.Updateprove(Convert.ToInt32(txtID.Text),txtName.Text, txtDirec.Text, mtbTelef.Text, txtCorreo.Text);
            this.Dispose();
        }
    }
}
