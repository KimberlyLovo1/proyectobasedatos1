﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoBD.BaseDatos;

namespace ProyectoBD.Add
{
    public partial class AddInven : Form
    {
        Validacion v = new Validacion();
        public AddInven()
        {
            InitializeComponent();
            loadCmb();
            btnActualiza.Visible = false;
            txtProve.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtDesc.Text.Equals("") || txtNameProd.Text.Equals("") || mtbPrecio.Text.Equals("") || mtbExist.Text.Equals("") || cmbProveedor.SelectedIndex == -1 || mtbCodProd.Text.Equals("") )
            {
                MessageBox.Show("Por favor, rellenar los campos");
                return;
            }
            
            int c = cmbProveedor.SelectedIndex + 1;
            PRODUCTO_DALC.InsertarProductos(txtDesc.Text, txtNameProd.Text, Convert.ToDouble(mtbPrecio.Text), Convert.ToInt32(mtbExist.Text), c, mtbCodProd.Text);
            reset();
        }

        public void loadCmb()
        {

            List<MostrarProveedores_Result> cargarProveedor = PROVEEDOR_DALC.CargarProveedores();
            foreach (MostrarProveedores_Result op in cargarProveedor)
            {
                cmbProveedor.Items.Add(op.ID_PROV + " " + op.NOMBRE_PROV);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        public void actualiza(String decp,String nomb,String precio, String Existencia, String prove,String cod)
        {
            btnActualiza.Visible = true;
            btnAceptar.Visible = false;
            txtNameProd.Text = nomb;
            mtbCodProd.Text = cod;
            mtbCodProd.Enabled = false;
            cmbProveedor.Visible = false;
            txtProve.Visible = true;
            txtProve.Text = prove;
            txtProve.Enabled = false;
            txtDesc.Text = decp;
            mtbPrecio.Text = precio;
            mtbExist.Text = Existencia;
        }
        public void reset()
        {
            txtDesc.Text = "";
            txtNameProd.Text = "";
            mtbCodProd.Text = "";
            mtbExist.Text = "";
            mtbPrecio.Text = "";
            cmbProveedor.SelectedIndex = -1;

        }

        private void mtbPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirNumerosDecimal(e);
        }

        private void txtNameProd_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirLetras(e);
        }

        private void mtbExist_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirEnteros(e);
        }

        private void btnActualiza_Click(object sender, EventArgs e)
        {
            if (txtDesc.Text.Equals("") || txtNameProd.Text.Equals("") || mtbPrecio.Text.Equals("") || mtbExist.Text.Equals("")  || mtbCodProd.Text.Equals(""))
            {
                MessageBox.Show("Por favor, rellenar los campos");
                return;
            }
            PRODUCTO_DALC.UpdateProducto(txtDesc.Text, txtNameProd.Text, Convert.ToDouble(mtbPrecio.Text), Convert.ToInt32(mtbExist.Text), Convert.ToInt32(txtProve.Text), mtbCodProd.Text);
            this.Dispose();
        }
    }
}
