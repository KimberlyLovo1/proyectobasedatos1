﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoBD.BaseDatos;

namespace ProyectoBD.Add
{
    public partial class AddCompra : Form
    {
        Validacion v = new Validacion();
        DateTime fech = DateTime.Now;
        int id_compra = COMPRA_DALC.Insertar_compra();

        public AddCompra()
        {
            InitializeComponent();
            loadcmbCod();
            mtbFechaCompra.Text = fech.ToString();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        public void loadcmbCod()
        {
            List<MostrarProductos_Result> CargarProducto = PRODUCTO_DALC.CargarProductos();
            foreach (MostrarProductos_Result mp in CargarProducto)
            {
                cmbCodProd.Items.Add(mp.COD_PROD);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        private void txtCant_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirEnteros(e);
        }

        private void txtPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirNumerosDecimal(e);
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (txtCant.Text.Equals("") || txtPrecio.Text.Equals(""))
            {
                MessageBox.Show("Ingrese los datos");
                return;
            }

            double sub = float.Parse(txtPrecio.Text) * float.Parse(txtCant.Text);

            for (int fila = 0; fila < dgvCompra.Rows.Count - 1; fila++)
            {
                if (dgvCompra.Rows[fila].Cells[1].Value.ToString() == cmbCodProd.SelectedItem.ToString())
                {
                    MessageBox.Show("Actualmente ya hay un producto con ese codigo, elimine y agregue otro");
                    return;
                }

            }
            dgvCompra.Rows.Add(cmbCodProd.SelectedItem.ToString(), txtPrecio.Text, txtCant.Text, sub.ToString());
            if (txtTotal.Text.Equals(""))
            {
                txtTotal.Text = sub.ToString();
            }
            else
            {
                sub = double.Parse(txtTotal.Text) + sub;
                txtTotal.Text = sub.ToString();
            }

        }

        private void btnFacturar_Click(object sender, EventArgs e)
        {
            if (dgvCompra.Rows.Count > 0)
            {
                int id_compra = COMPRA_DALC.Insertar_compra();
                if (id_compra == 0)
                {
                    MessageBox.Show("error intente nuevamente");
                    return;
                }

                for (int fila = 0; fila < dgvCompra.Rows.Count - 1; fila++)
                {

                    COMPRA_DALC.Insertar_compra_detalle(id_compra, dgvCompra.Rows[fila].Cells[0].Value.ToString(), Convert.ToInt32(dgvCompra.Rows[fila].Cells[2].Value.ToString()), float.Parse(dgvCompra.Rows[fila].Cells[1].Value.ToString()), float.Parse(dgvCompra.Rows[fila].Cells[3].Value.ToString()));
                }
                GASTOS_DALC.Update_er(laster(), 0, double.Parse(txtTotal.Text), 0, 0, 0);
                reset();
                MessageBox.Show("registrado correctamente");
                dgvCompra.Rows.Clear();


            }
        }
        public int laster()
        {
            int id = 0;
            List<MostrarER_Result> ep = GASTOS_DALC.listarEr();
            foreach (MostrarER_Result eop in ep)
            {
                id = eop.ID_ESTADO_RESULT;
            }
            return id;
        }

        public void reset()
        {
            txtCant.Text = "";
            txtPrecio.Text = "";
            txtTotal.Text = "";

        }

    }
}
