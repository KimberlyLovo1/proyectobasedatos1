﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoBD.BaseDatos;

namespace ProyectoBD.Add
{
    public partial class AddDeudores : Form
    {
        public AddDeudores()
        {
            InitializeComponent();
            btnActualizar.Visible = false;
            txtIDdeud.Visible = false;
        }
        public void Actualizar(string nomb,string dire,String idvent,String fecha)
        {
            btnActualizar.Visible = true;
            txtIDdeud.Visible = true;
            txtIDdeud.Enabled = false;
            btnAcept.Visible = false;
            txtId.Enabled = false;
            txtNomb.Text = nomb;
            txtDecp.Text= dire;
            txtId.Text = idvent;
            txtFech.Text = fecha;
            txtFech.Enabled = false;

        }

        private void btnAcept_Click(object sender, EventArgs e)
        {
            if (confirm() == 1)
            {
                DEUDORES_DALC.InsertarDeudores(txtNomb.Text, txtDecp.Text, Convert.ToInt32 (txtId.Text));
                this.Dispose();
                
            }

        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (confirm() == 1)
            {
                DEUDORES_DALC.UpdateDeudores(Convert.ToInt32(txtIDdeud.Text) ,txtNomb.Text, txtDecp.Text, Convert.ToInt32(txtId.Text));
                this.Dispose();

            }

        }

        public int confirm()
        {
            if(txtDecp.Text.Equals("") || txtFech.Text.Equals("") || txtNomb.Text.Equals(""))
            {
                MessageBox.Show("ERro rellene todos los datos");
                return 0;
            }
            return 1;
        }
      
    }
}
