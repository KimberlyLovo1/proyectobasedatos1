﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBD.Reporte
{
    public partial class View2 : Form
    {
        public View2()
        {
            InitializeComponent();
        }

        private void View2_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'bD_NGOCIO_TAMADataSet.PRODUCTO_MAS_VENDIDO' Puede moverla o quitarla según sea necesario.
            this.pRODUCTO_MAS_VENDIDOTableAdapter.Fill(this.bD_NGOCIO_TAMADataSet.PRODUCTO_MAS_VENDIDO);
            // TODO: esta línea de código carga datos en la tabla 'bD_NGOCIO_TAMADataSet1.ER_ULTIMO' Puede moverla o quitarla según sea necesario.
            this.eR_ULTIMOTableAdapter.Fill(this.bD_NGOCIO_TAMADataSet1.ER_ULTIMO);

            this.reportViewer1.RefreshReport();
        }
    }
}
