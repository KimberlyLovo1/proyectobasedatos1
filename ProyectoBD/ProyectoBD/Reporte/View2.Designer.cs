﻿namespace ProyectoBD.Reporte
{
    partial class View2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.bD_NGOCIO_TAMADataSet1 = new ProyectoBD.BD_NGOCIO_TAMADataSet1();
            this.eRULTIMOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.eR_ULTIMOTableAdapter = new ProyectoBD.BD_NGOCIO_TAMADataSet1TableAdapters.ER_ULTIMOTableAdapter();
            this.bD_NGOCIO_TAMADataSet = new ProyectoBD.BD_NGOCIO_TAMADataSet();
            this.pRODUCTOMASVENDIDOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pRODUCTO_MAS_VENDIDOTableAdapter = new ProyectoBD.BD_NGOCIO_TAMADataSetTableAdapters.PRODUCTO_MAS_VENDIDOTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.bD_NGOCIO_TAMADataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eRULTIMOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bD_NGOCIO_TAMADataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRODUCTOMASVENDIDOBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.pRODUCTOMASVENDIDOBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "ProyectoBD.Reporte.Report2.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(800, 450);
            this.reportViewer1.TabIndex = 0;
            // 
            // bD_NGOCIO_TAMADataSet1
            // 
            this.bD_NGOCIO_TAMADataSet1.DataSetName = "BD_NGOCIO_TAMADataSet1";
            this.bD_NGOCIO_TAMADataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // eRULTIMOBindingSource
            // 
            this.eRULTIMOBindingSource.DataMember = "ER_ULTIMO";
            this.eRULTIMOBindingSource.DataSource = this.bD_NGOCIO_TAMADataSet1;
            // 
            // eR_ULTIMOTableAdapter
            // 
            this.eR_ULTIMOTableAdapter.ClearBeforeFill = true;
            // 
            // bD_NGOCIO_TAMADataSet
            // 
            this.bD_NGOCIO_TAMADataSet.DataSetName = "BD_NGOCIO_TAMADataSet";
            this.bD_NGOCIO_TAMADataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pRODUCTOMASVENDIDOBindingSource
            // 
            this.pRODUCTOMASVENDIDOBindingSource.DataMember = "PRODUCTO_MAS_VENDIDO";
            this.pRODUCTOMASVENDIDOBindingSource.DataSource = this.bD_NGOCIO_TAMADataSet;
            // 
            // pRODUCTO_MAS_VENDIDOTableAdapter
            // 
            this.pRODUCTO_MAS_VENDIDOTableAdapter.ClearBeforeFill = true;
            // 
            // View2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.reportViewer1);
            this.Name = "View2";
            this.Text = "View2";
            this.Load += new System.EventHandler(this.View2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bD_NGOCIO_TAMADataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eRULTIMOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bD_NGOCIO_TAMADataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRODUCTOMASVENDIDOBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private BD_NGOCIO_TAMADataSet1 bD_NGOCIO_TAMADataSet1;
        private System.Windows.Forms.BindingSource eRULTIMOBindingSource;
        private BD_NGOCIO_TAMADataSet1TableAdapters.ER_ULTIMOTableAdapter eR_ULTIMOTableAdapter;
        private BD_NGOCIO_TAMADataSet bD_NGOCIO_TAMADataSet;
        private System.Windows.Forms.BindingSource pRODUCTOMASVENDIDOBindingSource;
        private BD_NGOCIO_TAMADataSetTableAdapters.PRODUCTO_MAS_VENDIDOTableAdapter pRODUCTO_MAS_VENDIDOTableAdapter;
    }
}