﻿namespace ProyectoBD
{
    partial class Tablas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DgvTable = new System.Windows.Forms.DataGridView();
            this.cmbAñadir = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTable)).BeginInit();
            this.SuspendLayout();
            // 
            // DgvTable
            // 
            this.DgvTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvTable.Location = new System.Drawing.Point(12, 41);
            this.DgvTable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DgvTable.Name = "DgvTable";
            this.DgvTable.RowHeadersWidth = 51;
            this.DgvTable.RowTemplate.Height = 24;
            this.DgvTable.Size = new System.Drawing.Size(776, 388);
            this.DgvTable.TabIndex = 0;
            this.DgvTable.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvTable_CellContentDoubleClick);
            this.DgvTable.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DgvTable_RowHeaderMouseDoubleClick);
            // 
            // cmbAñadir
            // 
            this.cmbAñadir.FormattingEnabled = true;
            this.cmbAñadir.Items.AddRange(new object[] {
            "Ventas",
            "Compras",
            "Inventario",
            "Proveedor",
            "Deudores",
            "gastos"});
            this.cmbAñadir.Location = new System.Drawing.Point(72, 11);
            this.cmbAñadir.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbAñadir.Name = "cmbAñadir";
            this.cmbAñadir.Size = new System.Drawing.Size(169, 24);
            this.cmbAñadir.TabIndex = 6;
            this.cmbAñadir.SelectedIndexChanged += new System.EventHandler(this.cmbAñadir_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Añadir:";
            // 
            // Tablas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbAñadir);
            this.Controls.Add(this.DgvTable);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Tablas";
            this.Text = "Visualización";
            ((System.ComponentModel.ISupportInitialize)(this.DgvTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DgvTable;
        private System.Windows.Forms.ComboBox cmbAñadir;
        private System.Windows.Forms.Label label1;
    }
}