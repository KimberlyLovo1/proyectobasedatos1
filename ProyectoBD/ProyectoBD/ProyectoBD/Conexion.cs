﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ProyectoBD
{
    class Conexion
    {
        public SqlConnection connect = new SqlConnection();

        public Conexion()
        {
            try
            {

                connect = new SqlConnection("Data Source=.;Initial Catalog=BD_NGOCIO_TAMA;Integrated Security=True");
                connect.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se falló" + ex.ToString());
            }
        }

        public void listarProductos(DataGridView GridView1)
        {

            SqlCommand cmd = new SqlCommand();
            SqlDataReader leer;

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "MostrarProductos";
            cmd.Connection = connect;


            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            da.Fill(dt);

            GridView1.DataSource = dt;

        }
        public void listarProveedores(DataGridView GridView1)
        {

            SqlCommand cmd = new SqlCommand();
            SqlDataReader leer;

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "MostrarProveedores";
            cmd.Connection = connect;


            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            da.Fill(dt);

            GridView1.DataSource = dt;

        }
        public void listarDeudores(DataGridView GridView1)
        {

            SqlCommand cmd = new SqlCommand();
            

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "MostrarDeudores";
            cmd.Connection = connect;


            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            da.Fill(dt);

            GridView1.DataSource = dt;

        }

        public void insertarProveedor(String nombre, String direccion, string telefono, String correo)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();

                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter("@nomb", SqlDbType.VarChar);
                param[0].Value = nombre;
                param[1] = new SqlParameter("@dir", SqlDbType.VarChar);
                param[1].Value = direccion;
                param[2] = new SqlParameter("@Telp", SqlDbType.Char);
                param[2].Value = telefono;
                param[3] = new SqlParameter("@correo", SqlDbType.VarChar);
                param[3].Value = correo;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ADD_PROVEEDOR";
                cmd.Connection = connect;
                cmd.Parameters.AddRange(param);

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(ds);
            }
            catch (Exception)
            {

                MessageBox.Show("Error en la insercion");
                return;
            }

        }

        public void ingresarProveedor(String nombre, String direccion, string telefono, String correo)
        {
            String query = "insert into INVENTARIO values(@nomb, @dir, @Telp, @correo)";

            try
            {

                connect = new SqlConnection("Data Source=.;Initial Catalog=BD_NGOCIO_TAMA;Integrated Security=True");
                connect.Open();
                SqlCommand comando = new SqlCommand(query, connect);
                comando.Parameters.AddWithValue("@nomb", nombre);
                comando.Parameters.AddWithValue("@dir", direccion);
                comando.Parameters.AddWithValue("@Telp", telefono);
                comando.Parameters.AddWithValue("@correo", correo);
                comando.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se falló" + ex.ToString());
            }
            
        }

    }
}
