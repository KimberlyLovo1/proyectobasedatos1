﻿using ProyectoBD.Add;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoBD.BaseDatos;

namespace ProyectoBD
{
    public partial class Tablas : Form
    {
        //Boolean prod, prov, deud;
        //Prod = identificador para verificar cual método utilizo (Productos)
        //Prov = proveedor, deud = deudores

        public Tablas()
        {
            InitializeComponent();

        }

        public void MostrarProductos()
        {

            DgvTable.DataSource = PRODUCTO_DALC.CargarProductos();
            btnProveedor.Visible = false;
            btnDeudores.Visible = false;
            btnVentas.Visible = false;
            btnCompra.Visible = false;
        }

        private void btnProveedor_Click(object sender, EventArgs e)
        {
            AddProv ap = new AddProv();
            ap.Visible = true;
            this.Dispose();

        }

        private void btnInventario_Click(object sender, EventArgs e)
        {
            AddInven ai = new AddInven();
            ai.Visible = true;
            this.Dispose();
        }

        private void btnCompra_Click(object sender, EventArgs e)
        {
            AddCompra ac = new AddCompra();
            ac.Visible = true;
            this.Dispose();
        }

        public void MostrarProveedores()
        {
            DgvTable.DataSource = PROVEEDOR_DALC.CargarProveedores();
            btnInventario.Visible = false;
            btnDeudores.Visible = false;
            btnCompra.Visible = false;
            btnVentas.Visible = false;
        }
        public void MostrarDeudores()
        {

            btnInventario.Visible = false;
            btnProveedor.Visible = false;
            btnCompra.Visible = false;
            btnVentas.Visible = false;
        }
        public void MostrarCompra()
        {
            DgvTable.DataSource = COMPRA_DALC.CargarCompras();
            btnInventario.Visible = false;
            btnProveedor.Visible = false;
            btnDeudores.Visible = false;
            btnVentas.Visible = false;
        }

        private void btnVentas_Click(object sender, EventArgs e)
        {
            AddVenta av = new AddVenta();
            av.Visible = true;
            this.Dispose();
        }

        public void MostrarVenta()
        {
            DgvTable.DataSource = VENTA_DALC.CargarVentas();
            btnInventario.Visible = false;
            btnProveedor.Visible = false;
            btnDeudores.Visible = false;
            btnCompra.Visible = false;
        }
    }
}
