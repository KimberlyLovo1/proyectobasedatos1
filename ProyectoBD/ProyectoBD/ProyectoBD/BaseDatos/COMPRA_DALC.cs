﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoBD.BaseDatos
{
    class COMPRA_DALC
    {
        public static List<MostrarCompra_Result> CargarCompras()
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.MostrarCompra().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<MOSTRAR_DETALLE_compra_Result> Cargar_DetalleCompras(int yawei)
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.MOSTRAR_DETALLE_compra(yawei).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static int Insertar_compra_detalle(int id, string cod, int cant, float costo, float subt)
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.IngresarCompra(id,cod,cant, costo,subt);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static int Insertar_compra()
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    var x = db.ADD_COMPRA(DateTime.Now).FirstOrDefault().ID_COMPRA;
                    return x;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

    }
}
