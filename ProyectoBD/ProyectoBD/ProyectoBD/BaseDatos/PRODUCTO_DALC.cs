﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoBD.BaseDatos
{
    class PRODUCTO_DALC
    {
        public static List<MostrarProductos_Result> CargarProductos()
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.MostrarProductos().ToList();
                }
            }catch(Exception)
            {
                throw;
            }
        }

        public static int InsertarProductos(String descrip, String nombre, double precio, int exist, int idProveedor, string codProd)
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.ADD_PRODUCTO(descrip, nombre, precio, idProveedor, codProd, exist);
                    //return db.ADD_PRODUCTO(descrip, nombre, precio, idProveedor, codProd);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
        
    }
}
