﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoBD.BaseDatos;

namespace ProyectoBD.Add
{
    public partial class AddCompra : Form
    {
        Validacion v = new Validacion();
        DateTime dia = DateTime.Now;
        int id_compra = COMPRA_DALC.Insertar_compra();

        public AddCompra()
        {
            InitializeComponent();
            loadcmbCod();
            mtbFechaCompra.Text = dia.ToString();
            //Añadir fechaaaaa porfa
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            
            if (txtCant.Text.Equals("") || txtPrecio.Text.Equals("") || mtbFechaCompra.Text.Equals("") || cmbCodProd.SelectedIndex == -1)
            {
                MessageBox.Show("Por favor, rellenar los campos");
                return;
            }
            
        }
        public void loadcmbCod()
        {
            List<MostrarProductos_Result> CargarProducto = PRODUCTO_DALC.CargarProductos();
            foreach (MostrarProductos_Result mp in CargarProducto)
            {
                cmbCodProd.Items.Add(mp.COD_PROD);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtSub_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCant_TextChanged(object sender, EventArgs e)
        {
            if (txtPrecio.Text.Equals(""))
            {
                
            }
            else
            {
                String subt = Convert.ToString((Convert.ToDouble(txtPrecio.Text)) * (Convert.ToDouble(txtCant.Text)));
                txtSub.Text = subt;
            }
        }

        private void txtPrecio_TextChanged(object sender, EventArgs e)
        {
            if (txtCant.Text.Equals(""))
            {

            }else
            {
                String subt = Convert.ToString((Convert.ToDouble(txtPrecio.Text)) * (Convert.ToDouble(txtCant.Text)));
                txtSub.Text = subt;
            }
            
        }

        private void txtCant_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirEnteros(e);
        }

        private void txtPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.AdmitirNumerosDecimal(e);
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            COMPRA_DALC.Insertar_compra_detalle(id_compra, cmbCodProd.Text, Convert.ToInt32( txtCant.Text), float.Parse(txtPrecio.Text), float.Parse(txtSub.Text));
            actualizar();
        }

        public void actualizar()
        {
            dgvCompra.DataSource = COMPRA_DALC.Cargar_DetalleCompras(id_compra);
        }
    }
}
