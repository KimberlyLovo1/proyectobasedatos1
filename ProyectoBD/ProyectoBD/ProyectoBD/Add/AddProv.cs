﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoBD.BaseDatos;

namespace ProyectoBD.Add
{
    public partial class AddProv : Form
    {
        public AddProv()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            //Conexion cn = new Conexion();
            //Char[] num = mtbTelef.Text.ToCharArray();
            //cn.ingresarProveedor(txtName.Text, txtDirec.Text, mtbTelef.Text, txtCorreo.Text);
            PROVEEDOR_DALC.InsertarProveedores(txtName.Text, txtDirec.Text, mtbTelef.Text, txtCorreo.Text);
            reset();
        }

        public void reset()
        {
            txtName.Text = "";
            txtDirec.Text = "";
            mtbTelef.Text = "";
            txtCorreo.Text = "";

        }
    }
}
