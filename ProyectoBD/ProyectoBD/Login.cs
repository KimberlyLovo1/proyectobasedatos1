﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoBD.BaseDatos;
using System.Threading;

namespace ProyectoBD
{
    public partial class Login : Form
    {
        conexion con;
        int cont = 3;
        public Login()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (txtUsuario.Text.Equals("") || txtContraseña.Text.Equals(""))
            {
                MessageBox.Show("No pueden haber campos vacios", "Alerta");
                Cursor.Current = Cursors.Default;
                return;
            }

            con = new conexion(txtUsuario.Text, txtContraseña.Text);
            if (this.con.cn.State == ConnectionState.Open)
            {
                MenuPrincipal mp = new MenuPrincipal();
                mp.Visible = true;

            }
            else
            {
                Cursor.Current = Cursors.Default;
                --cont;
                MessageBox.Show("Error:usuario o contrasenia incorrecta ", cont + " Intentos restantes");
                if (cont == 0)
                {
                    cont = 3;
                    btnAcep.Enabled = false;
                    Thread.Sleep(3000);
                    btnAcep.Enabled = true;


                }


            }
        }
    }
}
