﻿using ProyectoBD.Add;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoBD.BaseDatos;

namespace ProyectoBD
{
    public partial class Tablas : Form
    {
        Boolean vent, compra;
        Boolean prod, proveedor, deudore;
        //Prod = identificador para verificar cual método utilizo (Productos)
        //Prov = proveedor, deud = deudores

        public Tablas()
        {
            InitializeComponent();

        }

        public void MostrarProductos()
        {
            DgvTable.DataSource = PRODUCTO_DALC.CargarProductos();
            prod = true;
        }

        public void MostrarProveedores()
        {
            DgvTable.DataSource = PROVEEDOR_DALC.CargarProveedores();
            proveedor = true;
        }
        public void MostrarDeudores()
        {
            DgvTable.DataSource = DEUDORES_DALC.CargarDeudores();
            deudore = true;
        }
        public void MostrarCompra()
        {
            DgvTable.DataSource = COMPRA_DALC.CargarCompras();
            compra = true;
        }

        public void MostrarVenta()
        {
            DgvTable.DataSource = VENTA_DALC.CargarVentas();
            vent = true;
        }

        private void cmbAñadir_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbAñadir.SelectedIndex == 0)
            {
                AddVenta av = new AddVenta();
                av.Visible = true;
                this.Dispose();
            }
            if(cmbAñadir.SelectedIndex == 1)
            {
                AddCompra ac = new AddCompra();
                ac.Visible = true;
                this.Dispose();
            }
            if(cmbAñadir.SelectedIndex == 2)
            {
                AddInven ai = new AddInven();
                ai.Visible = true;
                this.Dispose();
            }
            if(cmbAñadir.SelectedIndex == 3)
            {
                AddProv ap = new AddProv();
                ap.Visible = true;
                this.Dispose();
            }
            if(cmbAñadir.SelectedIndex == 4)
            {
                AddDeudores ad = new AddDeudores();
                ad.Visible = true;
                this.Dispose();
            }
            if (cmbAñadir.SelectedIndex == 5)
            {
                AddGastos ep = new AddGastos();
                ep.Visible = true;
                this.Dispose();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void DgvTable_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
     
        }

        private void DgvTable_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (compra == true)
            {
               DgvTable.DataSource = COMPRA_DALC.Cargar_DetalleCompras( e.RowIndex + 1);
            }
            if (vent == true)
            {
               
               DgvTable.DataSource = VENTA_DALC.Cargar_detalleVentas(e.RowIndex + 1);
            }
            if (proveedor == true)
            {
                AddProv ep = new AddProv();
                ep.Actualiza(DgvTable.Rows[e.RowIndex].Cells[0].Value.ToString(), DgvTable.Rows[e.RowIndex].Cells[1].Value.ToString(), DgvTable.Rows[e.RowIndex].Cells[2].Value.ToString(), DgvTable.Rows[e.RowIndex].Cells[3].Value.ToString(), DgvTable.Rows[e.RowIndex].Cells[4].Value.ToString());
                ep.Visible = true;
                this.Dispose();

            }
            if (prod == true)
            {
                AddInven ep = new AddInven();
                ep.actualiza(DgvTable.Rows[e.RowIndex].Cells[0].Value.ToString(), DgvTable.Rows[e.RowIndex].Cells[1].Value.ToString(), DgvTable.Rows[e.RowIndex].Cells[2].Value.ToString(), DgvTable.Rows[e.RowIndex].Cells[3].Value.ToString(), DgvTable.Rows[e.RowIndex].Cells[4].Value.ToString(), DgvTable.Rows[e.RowIndex].Cells[5].Value.ToString());
                ep.Visible = true;
                this.Dispose();

            }
            if (deudore == true)
            {
                AddDeudores ep = new AddDeudores();
                ep.Actualizar(DgvTable.Rows[e.RowIndex].Cells[1].Value.ToString(), DgvTable.Rows[e.RowIndex].Cells[2].Value.ToString(), DgvTable.Rows[e.RowIndex].Cells[3].Value.ToString(), DgvTable.Rows[e.RowIndex].Cells[4].Value.ToString());
                ep.Visible = true;
                this.Dispose();
            }
        }
        public void gasto()
        {
            DgvTable.DataSource = GASTOS_DALC.MostrarGasto();
        }
    }
}
