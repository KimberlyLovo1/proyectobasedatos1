﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoBD.BaseDatos
{
    class DEUDORES_DALC
    {
        public static List<MostrarDeudores_Result> CargarDeudores()
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.MostrarDeudores().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static int UpdateDeudores(int idDeuda, String nombre, String direccion, int idVenta )
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    var x = db.UPDATE_DEUDORES(idDeuda, nombre, direccion, idVenta, DateTime.Now);
                    return x;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int InsertarDeudores(String Nombre, String Direcc, int idVenta)
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.ADD_DEUDORES(Nombre, Direcc, DateTime.Now, idVenta);

                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
