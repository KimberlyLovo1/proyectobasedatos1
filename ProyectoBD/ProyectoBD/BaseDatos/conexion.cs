﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ProyectoBD.BaseDatos
{
    class conexion
    {
        public SqlConnection cn;
        public conexion(string user, string pass)
        {
            try
            {
                cn = new SqlConnection("Server=LAPTOP-N1S7P978;Database=BD_NGOCIO_TAMA;UID=" + user + ";PWD=" + pass);

                cn.Open();


            }
            catch (Exception ex)
            {

                MessageBox.Show("Se falló" + ex.ToString());
            }
        }

        //confirma el tipo de usuario
        public static int Confirm(String pnomb)
        {
            int result;
            //especial por int out
            System.Data.Entity.Core.Objects.ObjectParameter myouputRole = new System.Data.Entity.Core.Objects.ObjectParameter("role", typeof(Int32));
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    db.sp_getrole(pnomb, myouputRole).FirstOrDefault();


                }
                result = Convert.ToInt32(myouputRole.Value);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
