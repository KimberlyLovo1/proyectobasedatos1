﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoBD.BaseDatos
{
    class GASTOS_DALC
    {
        public static List<MostrarGastos_Result> MostrarGasto()
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.MostrarGastos().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
        public static int InsertGastos(String nomb,String descp, String tipo_gast,float cost)
        {
            DateTime fech = DateTime.Now;
            fech.AddMonths(1);
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.AddGasto(nomb, descp, tipo_gast, cost, DateTime.Now, fech);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
        public static int Update_er(int id, double vent,double cost, double gastofin,double gastvent,double gastadm)
        {
            DateTime fech = DateTime.Now;
            fech.AddMonths(1);
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.UPDATE_ER(id,vent,cost,gastadm,gastvent,gastofin);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
        public static List<MostrarER_Result> listarEr()
        {
            DateTime fech = DateTime.Now;
            fech.AddMonths(1);
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.MostrarER().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
        public static int DefaultEr()
        {
            DateTime fech = DateTime.Now;
            fech.AddMonths(1);
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.DEFAULT_ER( DateTime.Now, fech);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        public static void confirm()
        {
            List<MostrarER_Result> ep = listarEr();
            if (ep.Count < 1)
            {
                DefaultEr();
            }
        }

    }
}
