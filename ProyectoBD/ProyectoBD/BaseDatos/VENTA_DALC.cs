﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoBD.BaseDatos
{
    class VENTA_DALC
    {
        public static List<MostrarVentas_Result> CargarVentas()
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.MostrarVentas().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<MOSTRAR_DETALLE_VENTA_Result> Cargar_detalleVentas(int id)
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.MOSTRAR_DETALLE_VENTA(id).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static int Insertar_venta(String nombre, String TipoPago)
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    
                    return db.ADD_VENTAS(nombre, DateTime.Now, TipoPago).FirstOrDefault().ID_VENTA;
                   
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int Insertar_Detalleventa(int idV, String codProd, double cant)
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    int x = 1;
                    db.ADD_DETALLE_VENTA(idV, codProd, cant);
                    return x;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
