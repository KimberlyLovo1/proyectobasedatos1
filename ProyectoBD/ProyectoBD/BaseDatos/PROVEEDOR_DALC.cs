﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoBD.BaseDatos
{
    class PROVEEDOR_DALC
    {
        public static List<MostrarProveedores_Result> CargarProveedores()
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.MostrarProveedores().ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static int InsertarProveedores(string nombre, string direc, string telef, string correo)
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.ADD_PROVEEDOR(nombre, direc, telef, correo);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static int Updateprove(int id,string nombre, string direc, string telef, string correo)
        {
            try
            {
                using (var db = new BD_NGOCIO_TAMAEntities())
                {
                    return db.UPDATE_Proveedores(id,nombre, direc, telef, correo);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }


    }
}
