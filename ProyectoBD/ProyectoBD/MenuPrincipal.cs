﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoBD.BaseDatos;
using ProyectoBD.Reporte;

namespace ProyectoBD
{
    public partial class MenuPrincipal : Form
    {
        
        public MenuPrincipal()
        {
            InitializeComponent();
            GASTOS_DALC.confirm();
            addNewER();
        }

        private void MenuPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            

        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Tablas prueba = new Tablas();
            prueba.MostrarProductos();
            prueba.Visible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Tablas prueba = new Tablas();
            prueba.MostrarProveedores();
            prueba.Visible = true;
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            Tablas prueba = new Tablas();
            prueba.MostrarDeudores();
            prueba.Visible = true;
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            Tablas prueba = new Tablas();
            prueba.MostrarCompra();
            prueba.Visible = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Tablas prueba = new Tablas();
            prueba.MostrarVenta();
            prueba.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            View1 ep = new View1();
            ep.Visible = true;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Tablas ep = new Tablas();
            ep.Visible = true;
            ep.gasto();
            
        }
        public void addNewER()
        {
            List<MostrarER_Result> ep = GASTOS_DALC.listarEr();
            if (ep.ElementAt(laster()-1).FECHA_FINAL < DateTime.Now)
            {
                GASTOS_DALC.DefaultEr();
            }
            
        }
        public int laster()
        {
            int id = 0;
            List<MostrarER_Result> ep = GASTOS_DALC.listarEr();
            foreach (MostrarER_Result eop in ep)
            {
                id = eop.ID_ESTADO_RESULT;
            }
            return id;
        }
    }
}
